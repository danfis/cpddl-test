_ clique_rand clique_biclique_rand lifted_mgroup_tests
ipc-1998/gripper/prob01
ipc-1998/gripper/prob10
ipc-1998/logistics/prob01
ipc-1998/movie/prob01
ipc-1998/movie/prob10
ipc-1998/mprime/prob01
ipc-1998/mprime/prob10 !h3 !trans_system
ipc-1998/mystery/prob01
ipc-1998/mystery/prob10 !h3 !trans_system
ipc-2000/elevators-adl/s10-0 !pddl_noce
ipc-2000/elevators-adl/s30-4 !pddl_noce !h3
ipc-2000/elevators-strips/s10-0
ipc-2000/elevators-strips/s30-4
ipc-2002/depot/pfile1
ipc-2002/depot/pfile10
ipc-2002/driverlog/pfile1
ipc-2002/driverlog/pfile10
ipc-2002/freecell/pfile1
ipc-2002/freecell/probfreecell-9-5 !h3
ipc-2002/rovers/pfile1
ipc-2002/rovers/pfile10
ipc-2002/zenotravel/pfile1
ipc-2002/zenotravel/pfile10
ipc-2004/airport-adl/p06-airport2-p2 !pddl_noce !strips_noce
ipc-2004/airport/p01-airport1-p1
ipc-2004/airport/p20-airport3-p7 !h2mgroup !h3
ipc-2004/pipesworld-notankage/p01-net1-b6-g2
ipc-2004/pipesworld-tankage/p01-net1-b6-g2-t50
ipc-2004/satellite/p01-pfile1
ipc-2006/pathways/p01
ipc-2006/pathways/p20 !h3
ipc-2006/pipesworld/p01-net1-b6-g2-t50
ipc-2006/pipesworld/p10-net1-b14-g8-t50 !h3
ipc-2006/rovers/p01
ipc-2006/rovers/p20 !h3
ipc-2006/storage/p01
ipc-2006/storage/p10
ipc-2006/tpp/p01
ipc-2006/tpp/p10
ipc-2006/trucks/p01
ipc-2006/trucks/p10 !h3
ipc-2008/seq-opt/elevators/p01
ipc-2008/seq-opt/elevators/p20
ipc-2008/seq-opt/openstacks/p01
ipc-2008/seq-opt/openstacks/p10
ipc-2008/seq-opt/parcprinter/p01
ipc-2008/seq-opt/parcprinter/p20 !h3
ipc-2008/seq-opt/pegsol/p01
ipc-2008/seq-opt/pegsol/p20
ipc-2008/seq-opt/scanalyzer/p01
ipc-2008/seq-opt/scanalyzer/p20 !h3 !trans_system
ipc-2008/seq-opt/sokoban/p01
ipc-2008/seq-opt/sokoban/p10
ipc-2008/seq-opt/transport/p01
ipc-2008/seq-opt/transport/p20 !h3 !trans_system
ipc-2008/seq-opt/woodworking/p01
ipc-2008/seq-opt/woodworking/p20 !h3
ipc-2008/seq-sat/cybersec/p10 !h3 !trans_system
ipc-2008/seq-sat/elevators/p10
ipc-2008/seq-sat/openstacks/p10
ipc-2008/seq-sat/sokoban/p10 !h3
ipc-2011/seq-opt/barman/pfile01-001
ipc-2011/seq-opt/barman/pfile03-010
ipc-2011/seq-opt/barman/pfile05-020
ipc-2011/seq-opt/elevators/p01
ipc-2011/seq-opt/elevators/p10
ipc-2011/seq-opt/elevators/p20
ipc-2011/seq-opt/floortile/opt-p01-001
ipc-2011/seq-opt/floortile/opt-p05-009
ipc-2011/seq-opt/floortile/opt-p10-020
ipc-2011/seq-opt/nomystery/p01
ipc-2011/seq-opt/nomystery/p10 !h3 !trans_system
ipc-2011/seq-opt/nomystery/p20 !h3 !trans_system
ipc-2011/seq-opt/openstacks/p01
ipc-2011/seq-opt/openstacks/p10
ipc-2011/seq-opt/openstacks/p20
ipc-2011/seq-opt/parcprinter/p01
ipc-2011/seq-opt/parcprinter/p10
ipc-2011/seq-opt/parcprinter/p20
ipc-2011/seq-opt/parking/pfile03-011
ipc-2011/seq-opt/parking/pfile06-021 !h3 !trans_system
ipc-2011/seq-opt/parking/pfile08-030 !h3 !trans_system
ipc-2011/seq-opt/pegsol/p01
ipc-2011/seq-opt/pegsol/p10
ipc-2011/seq-opt/pegsol/p20
ipc-2011/seq-opt/scanalyzer/p01
ipc-2011/seq-opt/scanalyzer/p10 !h3 !trans_system
ipc-2011/seq-opt/sokoban/p01
ipc-2011/seq-opt/sokoban/p10 !h3
ipc-2011/seq-opt/tidybot/p01
ipc-2011/seq-opt/tidybot/p10 !h3 !trans_system
ipc-2011/seq-opt/tidybot/p20 !h2mgroup !h3 !trans_system
ipc-2011/seq-opt/transport/p01
ipc-2011/seq-opt/transport/p10
ipc-2011/seq-opt/transport/p20 !trans_system
ipc-2011/seq-opt/visitall/problem02-full
ipc-2011/seq-opt/visitall/problem06-half
ipc-2011/seq-opt/visitall/problem11-full
ipc-2011/seq-opt/woodworking/p01
ipc-2011/seq-opt/woodworking/p10
ipc-2011/seq-opt/woodworking/p20
ipc-2011/seq-sat/parcprinter/p10 !h3
ipc-2014/seq-opt/barman/p435.1
ipc-2014/seq-opt/barman/p638.1
ipc-2014/seq-opt/barman/p839.1 !trans_system
ipc-2014/seq-opt/cavediving/testing01 !h3
ipc-2014/seq-opt/cavediving/testing10_easy !h3 !trans_system
ipc-2014/seq-opt/cavediving/testing20A_easy
ipc-2014/seq-opt/childsnack/child-snack_pfile01
ipc-2014/seq-opt/childsnack/child-snack_pfile05-2
ipc-2014/seq-opt/childsnack/child-snack_pfile10
ipc-2014/seq-opt/citycar/p2-2-2-1-2
ipc-2014/seq-opt/citycar/p2-3-3-3-1
ipc-2014/seq-opt/citycar/p3-3-3-3-2 !h3 !trans_system
ipc-2014/seq-opt/floortile/p01-4-3-2
ipc-2014/seq-opt/floortile/p02-5-4-2
ipc-2014/seq-opt/floortile/p03-6-5-2
ipc-2014/seq-opt/ged/d-1-2
ipc-2014/seq-opt/ged/d-4-2
ipc-2014/seq-opt/ged/d-8-9
ipc-2014/seq-opt/hiking/ptesting-1-2-3
ipc-2014/seq-opt/hiking/ptesting-2-3-5
ipc-2014/seq-opt/hiking/ptesting-2-4-7
ipc-2014/seq-opt/maintenance/maintenance.1.3.010.010.2-000
ipc-2014/seq-opt/maintenance/maintenance.1.3.015.020.2-002
ipc-2014/seq-opt/openstacks/p20_1
ipc-2014/seq-opt/openstacks/p35_3
ipc-2014/seq-opt/openstacks/p50_3 !h3
ipc-2014/seq-opt/parking/p_12_7-01
ipc-2014/seq-opt/parking/p_16_9-04 !h3 !trans_system
ipc-2014/seq-opt/parking/p_20_11-04 !h3 !trans_system
ipc-2014/seq-opt/tetris/p01-6
ipc-2014/seq-opt/tetris/p03-6
ipc-2014/seq-opt/tetris/p04-10 !h3 !trans_system
ipc-2014/seq-opt/tidybot/p01 !h3 !trans_system
ipc-2014/seq-opt/tidybot/p10 !h2mgroup !h3 !trans_system
ipc-2014/seq-opt/tidybot/p20 !h2mgroup !h3 !trans_system
ipc-2014/seq-opt/transport/p01
ipc-2014/seq-opt/transport/p10 !h3 !trans_system
ipc-2014/seq-opt/transport/p20 !h3 !trans_system
ipc-2014/seq-opt/visitall/p-05-10
ipc-2014/seq-opt/visitall/p-1-18 !h3 !trans_system
ipc-2014/seq-opt/visitall/p-1-5
ipc-2014/seq-sat/thoughtful/bootstrap-typed-01
ipc-2018/seq-opt/agricola/p01
ipc-2018/seq-opt/agricola/p20 !h3 !trans_system
ipc-2018/seq-opt/caldera/p03
ipc-2018/seq-opt/caldera-split/p01 !pddl_noce
ipc-2018/seq-opt/caldera-split/p10 !pddl_noce
ipc-2018/seq-opt/data-network/p01
ipc-2018/seq-opt/data-network/p20 !h3 !trans_system
ipc-2018/seq-opt/nurikabe/p01 !pddl_noce
ipc-2018/seq-opt/nurikabe/p10 !pddl_noce !symmetry !h3 !trans_system
ipc-2018/seq-opt/organic-synthesis/p01
ipc-2018/seq-opt/organic-synthesis/p10
ipc-2018/seq-opt/petri-net-alignment/p01 !h2mgroup !h3 !trans_system
ipc-2018/seq-opt/petri-net-alignment/p20 !h2mgroup !h3 !trans_system
ipc-2018/seq-opt/settlers/p01 !pddl_noce !strips_noce
ipc-2018/seq-opt/settlers/p10 !pddl_noce !strips_noce
ipc-2018/seq-opt/snake/p01 !h3 !trans_system
ipc-2018/seq-opt/snake/p20 !h3 !trans_system
ipc-2018/seq-opt/spider/p01 !h3 !trans_system
ipc-2018/seq-opt/spider/p20 !h2fwbw !h3 !trans_system
ipc-2018/seq-opt/termes/p01
ipc-2018/seq-opt/termes/p20
ipc-2018/seq-sat/flashfill/p10 !pddl_noce !strips_noce
unsolve-ipc-2016/bag-barman/prob01
unsolve-ipc-2016/bag-gripper/prob01 !strips !strips_dl !strips_sql !strips_noce !strips_prune
unsolve-ipc-2016/bag-transport/prob01
unsolve-ipc-2016/bag-transport/satprob20 !h2fwbw !h3 !trans_system
unsolve-ipc-2016/bottleneck/prob01
unsolve-ipc-2016/bottleneck/prob20 !h3 !trans_system
unsolve-ipc-2016/cave-diving/prob01
unsolve-ipc-2016/cave-diving/satprob02 !h3 !trans_system
unsolve-ipc-2016/chessboard-pebbling/prob03
unsolve-ipc-2016/chessboard-pebbling/prob20 !h3 !trans_system
unsolve-ipc-2016/diagnosis/prob01
unsolve-ipc-2016/diagnosis/satprob10
unsolve-ipc-2016/document-transfer/prob01 !trans_system
unsolve-ipc-2016/document-transfer/satprob01
unsolve-ipc-2016/over-nomystery/prob01
unsolve-ipc-2016/over-nomystery/satprob01
unsolve-ipc-2016/over-rovers/prob01
unsolve-ipc-2016/over-rovers/satprob01
unsolve-ipc-2016/over-tpp/prob01
unsolve-ipc-2016/over-tpp/satprob01
unsolve-ipc-2016/pegsol/prob05
unsolve-ipc-2016/pegsol-row5/prob05
unsolve-ipc-2016/pegsol-row5/satprob05 !h3 !trans_system
unsolve-ipc-2016/pegsol/satprob05
unsolve-ipc-2016/sliding-tiles/prob05
unsolve-ipc-2016/sliding-tiles/satprob05
unsolve-ipc-2016/tetris/prob05
unsolve-ipc-2016/tetris/prob10
various/flip/p01
various/miconic/p01 !pddl_noce
various/molgen/p01
various/schedule/probschedule-10-0 !pddl_noce !strips_noce
various/schedule/probschedule-51-0 !pddl_noce !strips_noce
various/wumpus/p01
